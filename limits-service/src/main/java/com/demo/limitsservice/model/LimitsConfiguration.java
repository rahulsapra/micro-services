package com.demo.limitsservice.model;

import lombok.Data;

@Data
public class LimitsConfiguration {

    private int maximum;
    private int minimum;

    public LimitsConfiguration(int maximum, int minimum) {
        this.maximum = maximum;
        this.minimum = minimum;
    }
}
