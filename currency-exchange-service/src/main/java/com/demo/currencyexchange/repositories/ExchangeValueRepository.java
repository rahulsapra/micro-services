package com.demo.currencyexchange.repositories;

import com.demo.currencyexchange.model.ExchangeValue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExchangeValueRepository extends JpaRepository<ExchangeValue, Long> {

    public List<ExchangeValue> findBySourceCurrencyAndTargetCurrency(String sourceCurrency, String targetCurrency);
}
