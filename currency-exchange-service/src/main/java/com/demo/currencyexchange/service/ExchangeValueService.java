package com.demo.currencyexchange.service;

import com.demo.currencyexchange.exceptions.ExchangeValueNotFoundException;
import com.demo.currencyexchange.model.ExchangeValue;
import com.demo.currencyexchange.repositories.ExchangeValueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExchangeValueService {

    @Autowired
    private ExchangeValueRepository exchangeValueRepository;

    public ExchangeValue findConversionRate(String sourceCurrency, String targetCurrency) throws ExchangeValueNotFoundException {
        List<ExchangeValue> exchangeValues = exchangeValueRepository.findBySourceCurrencyAndTargetCurrency(sourceCurrency, targetCurrency);
        if(exchangeValues.size() == 0) {
            throw new ExchangeValueNotFoundException(sourceCurrency, targetCurrency);
        }
        return exchangeValues.get(0);
    }
}
