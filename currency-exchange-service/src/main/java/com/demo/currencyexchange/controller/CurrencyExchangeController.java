package com.demo.currencyexchange.controller;

import com.demo.currencyexchange.exceptions.ExchangeValueNotFoundException;
import com.demo.currencyexchange.model.ExchangeValue;
import com.demo.currencyexchange.service.ExchangeValueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
@RequestMapping(path = "/currency-exchange")
public class CurrencyExchangeController {

    @Autowired
    private ExchangeValueService exchangeValueService;

    @GetMapping(path = "/from/{fromCurrency}/to/{toCurrency}")
    public ExchangeValue retrieveExchangeValue(@PathVariable(name = "fromCurrency") String fromCurrency,
                                               @PathVariable(name = "toCurrency") String toCurrency) throws ExchangeValueNotFoundException {

        return exchangeValueService.findConversionRate(fromCurrency, toCurrency);
    }
}
