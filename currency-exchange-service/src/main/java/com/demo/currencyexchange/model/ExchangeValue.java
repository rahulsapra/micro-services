package com.demo.currencyexchange.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;

@Entity
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ExchangeValue {

    @JsonIgnore
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @JsonProperty(value = "from")
    private String sourceCurrency;
    @JsonProperty(value = "to")
    private String targetCurrency;
    private BigDecimal conversionMultiple;

    public ExchangeValue() {
    }

    public ExchangeValue(String sourceCurrency, String targetCurrency, BigDecimal conversionMultiple) {
        this.sourceCurrency = sourceCurrency;
        this.targetCurrency = targetCurrency;
        this.conversionMultiple = conversionMultiple;
    }
}
