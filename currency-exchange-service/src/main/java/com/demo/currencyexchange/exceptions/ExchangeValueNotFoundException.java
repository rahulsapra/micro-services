package com.demo.currencyexchange.exceptions;

import com.demo.currencyexchange.util.Constants;

/**
 * custom exception class when no conversion rate is found from soureCurrency to targetCurrency
 */
public class ExchangeValueNotFoundException  extends  Exception{

    private String sourceCurrency;
    private String targetCurrency;

    public ExchangeValueNotFoundException(String sourceCurrency, String targetCurrency) {
        super(String.format(Constants.EXCHANGE_VALUE_NOT_FOUND, sourceCurrency, targetCurrency));
    }
}
