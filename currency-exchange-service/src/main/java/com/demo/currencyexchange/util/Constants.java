package com.demo.currencyexchange.util;

public class Constants {

    public static final String EXCHANGE_VALUE_NOT_FOUND = "No conversion rate found for the conversion %s to %s";
}
