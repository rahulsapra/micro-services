#Config-server application  
  
##tools used  
spring boot    
maven    
git  
  
##setting up local git repository  
*   create a folder to store config files    
        <em>mkdir local-git-config-repo</em>
*   go to the folder    
        <em>cd local-git-config-repo</em>
  
*   initialize empty git repo    
        <em>git init</em>  

*   create a property file in the folder. for application with name limits-sevice the file should be limits-service.properties

*   commit this added file to local repo.

*   In application.properties set the property spring.cloud.config.server.git.uri=file://<em>${path to local git repo}</em>        