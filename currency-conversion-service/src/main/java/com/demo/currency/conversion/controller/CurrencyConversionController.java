package com.demo.currency.conversion.controller;

import com.demo.currency.conversion.feignclients.CurrencyExchangeProxy;
import com.demo.currency.conversion.model.CurrencyConversionBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@RestController
public class CurrencyConversionController {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private CurrencyExchangeProxy currencyExchangeProxy;

    /**
     *  method using feign client
     * @param from
     * @param to
     * @param quantity
     * @return
     */
    @GetMapping(path = "/currency-converter-feign/from/{from}/to/{to}/quantity/{quantity}")
    public CurrencyConversionBean convertCurrencyFeign(@PathVariable String from, @PathVariable String to,
                                                  @PathVariable BigDecimal quantity) {
        CurrencyConversionBean currencyConversionBean = currencyExchangeProxy.retrieveExchangeValue(from, to);
        currencyConversionBean.setCalculatedAmount(
                quantity.multiply(currencyConversionBean.getConversionMultiple()));

        return currencyConversionBean;
    }


    @GetMapping(path = "/currency-converter/from/{from}/to/{to}/quantity/{quantity}")
    public CurrencyConversionBean convertCurrency(@PathVariable String from, @PathVariable String to,
                                                  @PathVariable BigDecimal quantity) {
        Map<String, String> uriVariables = new HashMap<>();
        uriVariables.put("from", from);
        uriVariables.put("to", to);

        ResponseEntity<CurrencyConversionBean> responseEntity =  restTemplate.getForEntity(
                "http://localhost:8000/currency-exchange/from/{from}/to/{to}",
                CurrencyConversionBean.class,
                uriVariables
        );

        CurrencyConversionBean currencyConversionBean = responseEntity.getBody();
        currencyConversionBean.setCalculatedAmount(
                quantity.multiply(currencyConversionBean.getConversionMultiple()));

        return currencyConversionBean;
    }
}
