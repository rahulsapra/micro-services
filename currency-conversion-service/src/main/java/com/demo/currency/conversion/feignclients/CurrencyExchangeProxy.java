package com.demo.currency.conversion.feignclients;

import com.demo.currency.conversion.model.CurrencyConversionBean;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

//@FeignClient(name = "currency-exchange-service", url = "http://localhost:8000")
//@FeignClient(name = "currency-exchange-service")
@FeignClient(name = "netflix-zuul-api-gateway-server")
@RibbonClient(name = "currency-exchange-service")
public interface CurrencyExchangeProxy {

    //    @GetMapping(path = "currency-exchange/from/{fromCurrency}/to/{toCurrency}")
    @GetMapping(path = "currency-exchange-service/currency-exchange/from/{fromCurrency}/to/{toCurrency}")
    public CurrencyConversionBean retrieveExchangeValue(@PathVariable(name = "fromCurrency") String fromCurrency,
                                                        @PathVariable(name = "toCurrency") String toCurrency);
}
